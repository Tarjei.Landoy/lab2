package INF101.lab2;

import java.util.List;
import java.util.ArrayList;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    List<FridgeItem> items;
    // I guess we 'haven't learned' about final yet...
    final int MAX_SIZE = 20;

    public Fridge() {
        items = new ArrayList<>();
    }

    @Override
    public int nItemsInFridge() {
        return items.size();
    }

    @Override
    public int totalSize() {
        return MAX_SIZE;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (items.size() >= MAX_SIZE) {
            return false;
        } else {
            items.add(item);
            return true;
        }

    }

    @Override
    public void takeOut(FridgeItem item) throws NoSuchElementException {
        if (items.size() < 1) {
            throw new NoSuchElementException();
        }

        items.remove(item);
    }

    @Override
    public void emptyFridge() {
        items.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredItems = new ArrayList<>();

        for (FridgeItem item : items) {
            if (item.hasExpired()) {
                expiredItems.add(item);
            }
        }

        items.removeAll(expiredItems);

        return expiredItems;
    }
}
